"""Dblog URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin

from article import views
from article import urls as article_urls
from loginsys import urls as loginsys_urls
from chat import urls as chat_urls

from django.views.generic import TemplateView


urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^basicview/1', views.basic_one),
    url(r'^basicview/2', views.template_two),
    url(r'^basicview/3', views.template_three),
    url(r'^auth/', include(loginsys_urls)),
    url(r'^articles/', include(article_urls)),
    url(r'^chat/', include(chat_urls)),
    url(r'^$', views.main_page),
    # url(r'^$', TemplateView.as_view(template_name="index.html"), name='index'),
]
