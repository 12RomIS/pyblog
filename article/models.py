from django.db import models

# Create your models here.

class Articles(models.Model):
    class Meta():
        db_table = "articles"
    article_title = models.CharField(max_length=200)
    article_text = models.TextField()
    article_date = models.DateTimeField()
    article_likes = models.IntegerField(default=0)


class Comments(models.Model):
    class Meta():
        db_table = "comments"

    comment_text = models.TextField(verbose_name="Текст комментария")
    comment_article = models.ForeignKey(Articles)
