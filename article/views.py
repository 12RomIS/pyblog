from django.core.exceptions import ObjectDoesNotExist
from django.shortcuts import render, render_to_response, redirect
from django.http.response import HttpResponse, Http404

# Create your views here.
from django.template import Context
from django.template.context_processors import csrf
from django.template.loader import get_template
from django.contrib import auth

from article.forms import CommentForm
from article.models import Articles, Comments



def basic_one(request):
    view = "basic_one"
    html = '<html><body>This is %s view</body></html>' %view
    return HttpResponse(html)


def template_two(request):
    view = "template_two"
    t = get_template('myview.html')
    html = t.render(Context({'name': view}))
    return HttpResponse(html)


def template_three(request):
    view = "template_three"
    return render_to_response('myview.html', {'name': view})


def main_page(request):

    return render_to_response('index.html', {
        'username': auth.get_user(request).username
    })



def articles(request):

    return render_to_response('articles.html', {
        'articles': Articles.objects.all(),
        'username': auth.get_user(request).username
    })


def article(request, article_id=1):

    args = {}
    args.update(csrf(request))
    args['article'] = Articles.objects.get(id=article_id)
    args['comments'] = Comments.objects.filter(comment_article_id=article_id)
    args['form'] = CommentForm
    args['username'] = auth.get_user(request).username

    return render_to_response('article.html', args)




def addlike(request, article_id):

    try:
        if article_id in request.COOKIES:
            redirect('/')
        else:
            article = Articles.objects.get(id = article_id)
            article.article_likes += 1
            article.save()
            response = redirect('/')
            response.set_cookie(article_id, 'test')
            return response
    except ObjectDoesNotExist:
        raise Http404
    return redirect('/')


def addcomment(request, article_id):
    if request.POST and ("pause" not in request.session):
        form = CommentForm(request.POST)
        if form.is_valid():
            comment = form.save(commit=False)
            comment.comment_article = Articles.objects.get(id=article_id)
            form.save()
            request.session.set_expiry(60)
            request.session['pause'] = True
    return redirect('/articles/get/'+article_id)

