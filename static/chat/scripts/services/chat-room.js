/**
 * Created by 12rom on 04.11.2016.
 */

'use strict';

angular.module('chatApp')

    .factory('ChatRoom', function(WebService) {
console.error('We are in chat-room.js - ChatRoom Service');
        return {
            getAll: function() {
                return WebService.get('chat-rooms/');
            },
            show: function(id) {
                return WebService.get('chat-rooms/' + id);
            },
            create: function(chatRoom) {
                return WebService.post('chat-rooms/create/', chatRoom);
            }
        }

    });