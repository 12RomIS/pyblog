/**
 * Created by Romis on 02.11.16.
 */


'use strict';

angular.module('chatApp', ['ngRoute'])

    .config(function($routeProvider){
console.error('we are in app.js');
        // console.log($routeProvider.when());
        $routeProvider

        .when('/chat-rooms', {
            templateUrl: '/static/chat/partials/chat-rooms.html',
            controller:  'ChatRoomsCtrl'
        })

        .when('/chat-room/:chatRoom', {
            templateUrl: '/static/chat/partials/chat-room.html',
            controller:  'ChatRoomCtrl'
        })

            .otherwise({
                templateUrl: '/static/chat/partials/404.html'
            });
    });