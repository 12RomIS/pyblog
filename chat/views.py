from django.http import HttpResponse, Http404
from django.shortcuts import render, render_to_response

from chat.models import ChatRooms, Messages
from chat.forms import ChatRoomForm

from django.core import serializers
from django.http import JsonResponse

# Create your views here.

# chat room actions
def get_all(request):

    objects = ChatRooms.objects.values('id', 'name')
    args = {}
    i = 0
    for item in objects:
        args[i] = item
        i+=1

    return JsonResponse(args)

    # objectQuerySet = ChatRooms.objects.all()
    # data = serializers.serialize('json', list(objectQuerySet), fields=('id', 'chat_rooms_name'))

    # serializers.serialize("json", ChatRooms.objects.all()), content_type = "application/json"
    # return HttpResponse( args )

    # return JsonResponse(
    #     ChatRooms.objects.values('id', 'chat_rooms_name', 'chat_rooms_date')
        # ChatRooms.objects.values_list('id', 'chat_rooms_name', 'chat_rooms_date')
        # list(ChatRooms.objects.all()),
    #     {
    #         0:{
    #             'id': 1,
    #             'chat_rooms_name': 'first',
    #             'date': '2017-01-13'
    #         }
    #     }
    # )

def show(request, chat_room_id):

    room = ChatRooms.objects.values('id', 'name').get(id=chat_room_id)
    return JsonResponse(room)


def create(request):

    # if request.GET:
    #     return HttpResponse(request.GET.get('name'))
    # else:
    #     return HttpResponse(request)


    #     return JsonResponse(ChatRooms.objects.filter(id=1))
    # form = ChatRoomForm(request.GET)
    # # if form.is_valid():
    form = ChatRoomForm(request.GET)
    if form.is_valid():
        room = form.save()
        return JsonResponse( room )
    else:
        return HttpResponse('else')



# messages actions
def get_by_chat_room(request):

    return '54545'


def create_in_chat_room(request):

    return '1515'


def get_updates(request, last_message_id, chatroom_id):

    objects = Messages.objects.values('id', 'name').filter(id>last_message_id, chatroom_id=chatroom_id)
    args = {}
    i = 0
    for item in objects:
        args[i] = item
        i += 1

    return JsonResponse(args)