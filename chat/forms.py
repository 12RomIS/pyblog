from django.forms import ModelForm
from chat.models import ChatRooms

class ChatRoomForm(ModelForm):
    class Meta():
        model = ChatRooms
        fields = ['name']