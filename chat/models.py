from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class ChatRooms(models.Model):
    class Meta():
        db_table = "chat_rooms"
    name = models.CharField(max_length=200)
    date = models.DateTimeField(auto_now_add=True)


class Messages(models.Model):
    class Meta():
        db_table = "messages"

    body = models.TextField(verbose_name="Your message")
    chat_room = models.ForeignKey(ChatRooms)
    user = models.ForeignKey(User)
    date = models.DateTimeField(auto_now_add=True)