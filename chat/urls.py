

from django.conf.urls import url
from chat import views
from django.views.generic import TemplateView

urlpatterns = [

    url(r'^api/chat-rooms/$', views.get_all),
    url(r'^api/chat-rooms/(?P<chat_room_id>\d+)$', views.show),
    url(r'^api/chat-rooms/create/$', views.create),

    url(r'^api/messages/(?P<chat_room_id>\d+)/$', views.get_by_chat_room),
    url(r'^api/messages/(?P<chat_room_id>\d+)/create/$', views.create_in_chat_room),
    url(r'^api/messages/(?P<last_messages>\d+)/(?P<chat_room_id>\d+)/$', views.get_updates),

    # url(r'^', views.main),
    url(r'^', TemplateView.as_view(template_name="chat/index.html"), name='index'),
]

